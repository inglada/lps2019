#+TITLE: Mapping France's land-cover at 10 m every year. Lessons learned and future improvements.
#+AUTHOR: Jordi INGLADA, Arthur VINCENT, Vincent THIERION

* Introduction
Since 2016, the French Theia Land Data Centre publishes every year a land cover map of metropolitan France at 10 m resolution. This map is produced by CESBIO using Sentinel-2 image time series, and the same approach has been applied to Landsat data to produce maps over the same area for the pre-Sentinel years. The nomenclature contains the following 17 classes: Continuous urban fabric; Discontinuous urban fabric; Industrial or commercial units; Road surfaces; Annual summer crops; Annual winter crops; Intensive grassland; Orchards; Vineyards; Broad-leaved forest; Coniferous forest; Natural grasslands; Woody moorlands; Beaches, dunes and sand plains; Bare rock; Glaciers and perpetual snow; Water bodies.

Three years of operational production allows us to propose an interesting retrospective integrating feedback from both the producer and the user's point of view. In this talk, we will present the lessons learned and give an overview of the ongoing and future work for improving the quality and usefulness of the product.

* Methodology
The approach used is based on supervised classification of Sentinel-2 image time series and has been described in detail in [fn:1]. The reference data used for training the machine learning models comes from a variety of existing data source as Corine Land Cover, Urban Atlas or the French LPIS. Since these databases may be out of date  and have coarser spatial resolutions than the target map, a specific pre-processing workflow has been designed. 

The main difficulty for large area mapping comes from the data volumes and their heterogeneous quality across the different regions. Indeed, several orbits are needed to cover the whole area, which implies that different areas have different acquisition dates. Furthermore, cloud cover is heterogeneous. All this amounts to having time series with different number of time stamps and temporal grids. The adopted approach to use all available data (no image is discarded unless it has more than 90% cloud cover) and resample them on to a common time grid. Cloud and cloud shadow masks are used to decide whether a pixel at a particular date is valid or not. In this way, a cleansed and homogeneous data cube is produced and fed to the machine learning step.

In order to take into account climatic variability between regions, a spatial stratification is performed using eco-climatic areas. A different classifier is trained for each climatic area allowing to improve the overall accuracy by an amount close to 5%.

The production workflow is fully automatic and is implemented in the ~iota2~ open source processing chain [fn:2].

* Product validation
The validation of the product is crucial for end-users to integrate the maps in their workflows. Several levels of validation are performed. The first one is a classical machine learning validation which uses a hold-out data set extracted from the reference data and that is not used for training. This allows to produce detailed confusion matrices and derived metrics as overall accuracy, kappa index and per-class metrics as the FScore. This validation allows to check that the machine learning approach has good performances and that over-fitting is avoided. However, due to the errors present in the reference data, disagreements between the map and the reference data may be difficult to interpret. Furthermore, biases in the reference data could be present in the produced map and would not be detected [fn:3]. 

In order to complement this machine learning validation, independent smaller data sets are also used. These datasets come from field surveys, partner institutes and end users. They are small and local by construction and can have different nomenclatures. Therefore, a specific validation procedure including nomenclature translation and class regrouping needs to be used. The results are coherent with the classical machine learning validation and confirm the good overall accuracy of the maps and also the main confusions between similar land cover classes [fn:4].

A third kind of validation, performed by professional photo-interpreters was also performed for the 2016 map. A fully independent validation following a sound protocol is costly and needs skills and expertise that are very specific. SIRS is a company which is specialised in the production of geographic data from satellite or aerial images. Among other things, they are the producers of Corine Land Cover for France and they are also responsible for quality control and validation of other Copernicus Land products. The analysis showed that the product meets the usually accepted thematic validation requirements for operational Copernicus products [fn:5].

* Main limitations and user feedback
Despite the good conclusions of the different validation approaches, there are still many issues with the delivered products. 

First of all, some individual classes are poorly detected and there are strong confusions between some classes of the same category. For instance, continuous and discontinuous urban fabric are sometimes difficult to split. The same kind of problem occurs with grasslands and moorlands, mostly in the gradient areas between the two. The origin of these confusions is twofold: 
1. the classes are very similar;
2. the reference data imposes arbitrary boundaries depending on the area and the context. 

The first issue needs better algorithms, the second needs better reference data.

Second of all, many users ask for a more detailed nomenclature, mainly in croplands and wetlands. As a consequence, a new nomenclature made up of 30 classes instead of 17 has been designed and is starting to be tested. The first results are encouraging, but new algorithmic developments will be needed.

Finally, producing a new map once a year invites users to analyse land cover changes. Depending on the scale of the change analysis, our maps may not be appropriate. While the trends over large areas seem to be reliable, the land cover evolution analysis at the pixel level is not possible with the current maps. Indeed, comparing 2 maps which have 90% accuracy does not allow to look for changes which have occurrences much lower than 10%. In order to help users interpret the changes, a confidence map is also distributed. This map gives, for every pixel, a confidence index of the decision of the classifier is provided. This index can therefore be used to weight the changes detected between 2 maps. Future versions of the map will be accompanied by the probability of each class, in order to allow more detailed analysis of the observed changes. Nevertheless, this additional information is not a solution, and better accuracies should be achieved to grant access to robust change information.

* What's next
In order to improve the quality of the maps and their usefulness for end users, several research activities are ongoing. The first one, already pointed out above, is the extension of the nomenclature to 30 classes to fulfil users' demands. This nomenclature will be presented in the talk. Added to that, the following algorithmic improvements are being addressed:

1. In order to reduce confusions between similar classes, a contextual classification approach needs to be implemented. Indeed, the high spatial resolution of Sentinel-2 reveals textures and local pixel distributions which are meaningful for the classification. Deep learning approaches and particularly Convolutional Neural Networks seem appropriate for the task. However, their computational cost may be to high for operational mapping at scale. Other aspects as the need for dense annotations and nearly error-free reference data hinders their use in our application. In this context we work on the development of contextual classification with shallow learning. The first results show that these approaches are competitive with Deep learning in terms of accuracy and much cheaper in terms of computation.
2. Since some of the errors in the maps come from the fact that the reference data used may be out of date, techniques allowing to train the classifiers with fewer amounts of reference data for some classes are interesting. We are developing domain adaptation techniques which allow to use reference and image data from previous periods to leverage supervised classification using outdated reference data.
3. In order to reduce the false alarms in change detection, the use of imagery spanning several years in the past is of great help, since it allows to regularise the classification in the temporal dimension.
4. In order to improve the maps in areas with frequent cloud cover, our processing chain is able to jointly use Sentinel-1 and Sentinel-2 time series. However, our experiments up to date do not show significant increase in accuracy. This may be mainly due to the increase of dimensionality of the problem. In this regard, we need to investigate better approaches for the use of multi-modal imagery.

* Notes                                                               :noexport:
** Sessions
*** A3.08 Land Cover Regional to Global

**** Regular Contributed
**** Description: 
With its Earth Observation programmes the Agency has supported the prototyping of Global Land Cover products since the launch of ENVISAT with GlobCover 2005 and GlobCover 2009. Then with its Climate Change Initiative the series has been consistently extended back with the AVHRR to 1992 up to 2015 with Proba-V at medium resolution. Now with the advent of full operational capacity of the sentinel series, another arena is open. The space capabilities to support more systematic regional development and production of High Resolution Regional Land Cover exists and has been demonstrated with the 2016 African prototype at 20 meters for 2016 and the Mexican prototype for 2017 at 10 meters. Alternatives approaches often targeting single land cover class have been also recently completed at global scale in various international frameworks. This session intends to capitalize on the progress performed within the science community on this subject and will be the occasion to present new results on Regional and Worldwide Land Cover and potentially trigger new research initiatives.

**** Convenors: Lorenzo Bruzzone (U. Trento), Pierre Defourny (U.C. Lovain)
*** A3.09 Next generation land cover monitoring services: Towards a flexible, user-oriented approach.

**** Regular Contributed
**** Description: 
During the last decade several global land cover datasets have been created at medium spatial resolution. For instance, the GLC-2000 (1 km), MCD12Q1 (500 m) and GlobCover and CCI Landcover (300 m) products. Recent developments in different fields provide vast new potential for global land cover mapping and change detection. New global datasets based on Earth Observation (EO) satellite data are now becoming available and -often- freely accessible. These datasets, with higher spatial resolution (e.g Sentinel-2, Landsat, PROBA-V, but also very high resolution missions) could significantly improve the characterization of the Earth’s land surface and provide land cover maps at a finer scale. Moreover, the new data available in combination with more powerful computing infrastructure and advances in deep learning/artificial intelligence techniques allow for on-demand, adaptable land cover mapping, hence going beyond Land cover 2.0 (Wulder et al, 2018). 
Land cover data and maps are used by various user groups to better understand the current landscape and the human impact on these landscapes. Next to be a core information layer for a variety of scientific and administrative tasks, maps can help organizations, stakeholders, politicians and individual users to assess urban growth, assist in land management and land planning, track wetland losses and potential impacts from sea level rise, prioritize areas for conservation efforts, assess the impact of climate change on socio-economic, socio-ecologic systems as well as ecosystem services, etc. This variety of different user groups and their different needs make it hard to create a one fits all land cover map. As a result most detailed land cover maps end up with a lot of mixed land cover classes which are hard to be used in different applications. Hence, we have to move towards flexible and user-oriented land cover maps. 
But there is more, currently there is no single mission or sensor that can combine fine spatial, spectral and temporal resolution. It is obvious that a combination of data originating from missions with either a high spectral, spatial or temporal resolution will result in more accurate land cover maps, which again should be tailored to the needs of a specific user group, e.g. Ecosystem Natural Capital Accounting. 
Based on these various aspects we would like to host a session which discusses innovations in operational land cover mapping and new methodologies on how to address different user communities. We believe that it’s time to have a good discussion on this subject with all the new technologies, data sources and possibilities that currently exist or will emerge in near-future. 

**** Convenors: Ruben Van De Kerchove (VITO), Martin Herold (U. Wageningen), Michael A. Wulder (NRCan)
** [2018-11-01 Thu 20:17] lessons learned when producing every year
- the changes
- the validation
- updating previous maps
- integrating user feedback
- updating algorithms
- our research and development programme for global land cover at 10m resolution 
** phiweek
Mapping continuous (bio-/geophysical) or categorical (land-cover) variables is of paramount importance for science and operational applications. The availability of open Copernicus data and recent advances in machine learning (ML) have opened the door to implementing high resolution global mapping systems in nearly-real-time cite:gomez16:_optic. The literature is filled with exciting results combining Deep Learning (DL) and big data cite:kussul17_deep_learn_class_land_cover, cite:cai18_high_perfor_in_season_class, mostly for agriculture. However, most of available products over large areas are based on classical ML approaches using limited amounts of data and guided by human expert knowledge. This situation is not merely due to a time lag between research and operations neither is computing and storage infrastructure the only bottleneck for the implementation of these modern approaches. Big internet firms' clouds have been available for several years. Although some global maps have been produced using those cite:pekel16_high_resol_mappin_global_surfac, cite:gorelick17_googl_earth_engin, they are mostly based on simple algorithms like threshold-based approaches or decision trees instead of DL, for which these internet firms are the main players.

This isn't a surprise since most DL approaches have scalability limitations when moving from the computer vision problems, for which they were designed, to the remote sensing (RS) field. The main issues are the computational complexity and the need for labelled training data. 

The computational complexity is much higher mainly because the feature space in RS is several orders of magnitude higher than the RGB space of computer vision. A year of Sentinel-2 data (for phenology characterisation, for instance) amounts to 720 variables per pixel (10 bands every 5 days). Adding contextual information can dramatically increment this dimensionality and DL approaches have thousands of parameters to be optimised per image dimension. Current strategies for applying DL to this kind of time series consist in reducing the number of dates or bands, or sharing parameters among dimensions. Also, DL approaches used in RS tend to be rather shallow and only the contextual (convolutional) aspect of DL is exploited. 

DL approaches also need large amounts of labelled training data, but reference data is costly to obtain in RS applications. Furthermore, DL usually needs densely annotated data (all the pixels of the training area), while reference data in RS applications is often collected on individual points or sparse polygons.

The case of Onesoil.ai https://map.onesoil.ai/ where they limit to LPIS (they have the geometry and classify per object= --> this is a simplified problem

Finally, the choice of a ML approach for mapping should be also made in terms of the interpretability of the model. Although some approaches for visualising what happens inside a neural network exist, they don't achieve the interpretability of the weights of a logistic regression or the thresholds of a decision tree. 

At the end of the day, the choice between a small improvement in accuracy at the cost of much higher computation costs, the need of expensive reference data collection and the inability to understand the model has to be made for each application and use case. In this presentation, we will discuss how a middle ground between classical ML approaches (with worse accuracy) and recent DL techniques (having much higher costs) for global mapping at metric resolution could be achieved.

** [2018-10-29 Mon 19:56] land cover mapping is solved : what's next :noexport:
- existing products
- gap between publicly available products (bad), litterature (medium), commercial (good)
- methods
  - change dtection
  - new classes with no training data
  - semantic drift
  - queries with definitions, but no examples : back to ontologies?* Configs
  - the generic workflow for any kind of data?
- scientific questions, thematic areas
  - wetlands?
  - wayback machine
    - no data was collected at the correct resolution
    - data quality is lower
      - SWH vs S2
- infrastructure? -> out of scope

** cesbio.eu
vnt2lps11
* Footnotes

[fn:1] Inglada, J., Vincent, A., Arias, M., Tardy, B., Morin, D., & Rodes, I., Operational high resolution land cover map production at the country scale using satellite image time series, Remote Sensing, 9(1), 95 (2017).  http://dx.doi.org/10.3390/rs9010095

[fn:2] https://framagit.org/inglada/iota2

[fn:3] http://www.cesbio.ups-tlse.fr/multitemp/?p=12630

[fn:4] http://www.cesbio.ups-tlse.fr/multitemp/?p=11835

[fn:5] http://www.cesbio.ups-tlse.fr/multitemp/wp-content/uploads/2018/02/SIRS_CESBIO_OSO_Validation_Report_v1.3.pdf


